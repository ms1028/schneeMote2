# schneemote 2.0

RaspberryPi 433Mhz remote control + alarm clock written in django.

Supports multiple sending devices for better signal coverage.

# Warning!!

This is a very early version!

# ToDo until first release:

- add logs. schneemote didnt work tokay -and i can not debug it- (problem was caused by los signal on the physical layer)
- Write tutorial for sending devices (raspberry pis)
- Build docker container with controller software (web ui)
- Write configuration tutorial
- Add multi language model
- Build interface for configuration parameters (time-zone)
- Password protection
- clean up the project

# ToDo until final version:

- Build better forms (not only djangos default admin interfaces)
- better documentation

# Links:

Screenshots: https://gitlab.com/ms1028/schneeMote2/wikis/screenshots

Install: https://gitlab.com/ms1028/schneeMote2/wikis/install

Config: https://gitlab.com/ms1028/schneeMote2/wikis/config

Wiki: https://gitlab.com/ms1028/schneeMote2/wikis/home

# Recommended reading: