from django.db import models
from django.contrib import admin
import socket
from alarmevents.models import repetitiveAlarm

# Create your models here.
class Sender433Mhz(models.Model):
	name = models.CharField(max_length=15)
	ip = models.CharField(max_length=15)
	port = models.IntegerField()

	def send(self, msg):
		try:
			sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			sock.settimeout(5)
			sock.connect((self.ip, self.port))
			msg = msg.encode('utf-8')
			sent = sock.send(msg[0:])
			sock.close()
			return True
		except ConnectionRefusedError:
			return False
			...#todo: log
		except Exception:
			return False

	def isOnline(self):
		try:
			sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			sock.settimeout(5)
			sock.connect((self.ip, self.port))
			sock.close()
			return True
		except ConnectionRefusedError:
			return False
		except TimeoutError:
			return False
		except Exception:
			return False

	def __str__(self):
		return self.name

class Room(models.Model):
	name = models.CharField(max_length=160)
	description = models.CharField(max_length=1200)

	def __str__(self):
		return self.name

class MhzDevice433(models.Model):
	name = models.CharField(max_length=160)
	description = models.CharField(max_length=1200)

	room = models.ForeignKey(Room)

	system_code = models.CharField(max_length=5)
	unit_code = models.CharField(max_length=2)

	def send(self,msg):
		senders = Sender433Mhz.objects.all()
		result = []
		for sender in senders:
			if not sender.send(msg):
				result.append(sender.name)

		return result

	def send_on(self):
		msg = self.system_code + self.unit_code + "1"
		return self.send(msg)

	def send_off(self):
		msg = self.system_code + self.unit_code + "0"
		return self.send(msg)

	def __str__(self):
		return self.name

class SwitchAction(models.Model):
	event = models.ForeignKey(repetitiveAlarm)
	device = models.ForeignKey(MhzDevice433)
	active = models.BooleanField(default=True)