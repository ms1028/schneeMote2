from django.contrib import admin
# Register your models here.
from .models import repetitiveAlarm
from devices.models import SwitchAction


class SwitchActionInline(admin.StackedInline):
    model = SwitchAction
    extra = 1


class AlarmAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['description', 'enabled']}),

        (None, {'fields': ['hours', 'minutes']}),

        ("Repeat on ... ", {'fields': ['monday',
                                       'tuesday',
                                       'wednesday',
                                       'thursday',
                                       'frieday',
                                       'saturday',
                                       'sunday'],
                            'classes': ['collapse']}),
    ]
    inlines = [SwitchActionInline]


admin.site.register(repetitiveAlarm, AlarmAdmin)
