from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render

from devices.models import MhzDevice433, repetitiveAlarm, SwitchAction, Sender433Mhz, Room


# Create your views here.
# @login_required
def index(request):
    device_list = MhzDevice433.objects.select_related().all()
    room_list = Room.objects.all()
    active = "all"

    device_pk = request.POST.get("device_pk", "")
    switch = request.POST.get("switch", "")
    room = request.REQUEST.get("room", "")
    switch_all = request.POST.get("switch_all", "")
    do_send = False
    sending_result = ""

    if device_pk != "" and switch != "":
        do_send = True
        switch_me = device_list.get(pk=device_pk)
        if switch == "on":
            sending_result = switch_me.send_on()
        if switch == "off":
            sending_result = switch_me.send_off()

    if switch_all == "True" and switch != "":
        do_send = True
        if room != "all":
            switch_mes = device_list.filter(room=room)
        else:
            switch_mes = device_list

        for switch_me in switch_mes:
            if switch == "on":
                sending_result = switch_me.send_on()
            if switch == "off":
                sending_result = switch_me.send_off()

    if room != "all" and room != "":
        active = room
        device_list = device_list.filter(room=room)

    context = {'device_list': device_list,
               'do_send': do_send,
               'result': sending_result,
               'room_list': room_list,
               'active_name': room_list.get(pk=room).name if active != "all" else "null",
               'active': active}

    return render(request, 'remote.html', context)


def alarm_table(request):
    alarms = repetitiveAlarm.objects.all()
    actions = SwitchAction.objects.all()

    alarms_detailed = [{'alarm': alarm, 'actions': actions.filter(event=alarm.pk)} for alarm in alarms]
    context = {'alarms': alarms_detailed}
    return render(request, 'alarm_table.html', context)


def senders_table(request):
    senders = Sender433Mhz.objects.all()
    senders_with_status = [{'name': sender.name, 'ip': sender.ip, 'port': sender.port, 'status': sender.isOnline()} for
                           sender in senders]
    context = {'senders': senders_with_status, 'debugging': True}
    return render(request, 'senders_table.html', context)


def send_manuell(request):
    systemcode = request.GET.get("systemcode", "")
    device = request.GET.get("device_number", "")
    sender = request.GET.get("sender", "")
    switch = request.GET.get("switch", "")
    sending_result = ""
    do_send = False

    if systemcode != "" and device != "" and sender != "" and switch != "":
        do_send = True
        sender433 = Sender433Mhz.objects.all().get(pk=sender)
        msg = systemcode + device + switch
        sending_result = sender433.send(msg)

    senders = Sender433Mhz.objects.all()
    context = {'senders': senders, 'do_send': do_send, 'sending_result': sending_result}
    return render(request, 'send_manuell.html', context)
