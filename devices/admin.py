from django.contrib import admin

# Register your models here.
from .models import MhzDevice433, Room, Sender433Mhz, SwitchAction

admin.site.register(MhzDevice433)
admin.site.register(Room)
admin.site.register(Sender433Mhz)