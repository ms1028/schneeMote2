from django.db import models


# Create your models here.
class Alarm(models.Model):
    description = models.CharField(max_length=1200)
    hours = models.IntegerField()
    minutes = models.IntegerField()
    enabled = models.BooleanField(default=True)


class repetitiveAlarm(Alarm):
    monday = models.BooleanField(default=True)
    tuesday = models.BooleanField(default=True)
    wednesday = models.BooleanField(default=True)
    thursday = models.BooleanField(default=True)
    frieday = models.BooleanField(default=True)
    saturday = models.BooleanField(default=True)
    sunday = models.BooleanField(default=True)

    def __str__(self):
        return self.description
