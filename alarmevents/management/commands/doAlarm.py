import datetime

from django.core.management import BaseCommand
from pytz import timezone

from alarmevents.models import repetitiveAlarm
from devices.models import SwitchAction


# The class must be named Command, and subclass BaseCommand
class Command(BaseCommand):
    # Show this when the user types help
    help = "checks current time and starts (if needed) an alarm"

    # A command must define handle()
    def handle(self, *args, **options):
        self.stdout.write("-------------------------------")
        utc_time = datetime.datetime.utcnow().replace(tzinfo=timezone('UTC'))
        lt = utc_time.astimezone(timezone('Europe/Berlin'))
        hours_lt = lt.strftime("%H")
        minutes_lt = lt.strftime("%M")
        weekday = datetime.datetime.today().weekday()
        self.stdout.write("time is " + hours_lt + ":" + minutes_lt + " day is " + str(weekday))

        self.stdout.write("checking for current alarms ...")
        alarms_to_fire = repetitiveAlarm.objects.all()\
            .filter(hours=hours_lt)\
            .filter(minutes=minutes_lt)\
            .filter(enabled=True)

        if weekday == 0:
            alarms_to_fire = alarms_to_fire.filter(monday=True)
        elif weekday == 1:
            alarms_to_fire = alarms_to_fire.filter(tuesday=True)
        elif weekday == 2:
            alarms_to_fire = alarms_to_fire.filter(wednesday=True)
        elif weekday == 3:
            alarms_to_fire = alarms_to_fire.filter(thursday=True)
        elif weekday == 4:
            alarms_to_fire = alarms_to_fire.filter(frieday=True)
        elif weekday == 5:
            alarms_to_fire = alarms_to_fire.filter(saturday=True)
        elif weekday == 6:
            alarms_to_fire = alarms_to_fire.filter(sunday=True)

        self.stdout.write(str(len(alarms_to_fire)) + " alarms are ready to fire!")

        for fire_me in alarms_to_fire:
            switch_mes = SwitchAction.objects.all().filter(event=fire_me)
            self.stdout.write("\t" + fire_me.__str__() + "requests " + str(len(switch_mes)) + " actions")

            for switch_me in switch_mes:
                if switch_me.active == True:
                    self.stdout.write("\t\t fire! " + switch_me.device.__str__() + " on!")
                    switch_me.device.send_on()
                if switch_me.active == False:
                    self.stdout.write("\t\t fire! " + switch_me.device.__str__() + " off!")
                    switch_me.device.send_off()
