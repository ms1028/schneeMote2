#!/usr/bin/env python
 
import socket

class SendSocket:
	
	def __init__(self):
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.host = "192.168.1.3"
		self.port = 11337

	def connect(self,host,port):
		self.sock.connect((host, port))

	def send(self, msg):
		msg = msg.encode('utf-8')
		self.connect(self.host,self.port)
		sent = self.sock.send(msg[0:])
		self.sock.close()